
const getAgeClient = require('../helper/getAgeClient');
const { randomNumber } = require('../helper/randomNumber');
const {CreateClientEventValidation} = require('../schema/input/createClientEvent.input');
const { updateClientService } = require('../service/updateClient.service');

const createCardDomain = async (payload, meta) => {
  // valido el schema
  const validations = new CreateClientEventValidation(JSON.parse(payload.Message),meta);
  const schema = validations.get();

  //armo las propiedades de la tarjeta 
  const creditCardNumber = `${randomNumber(0, 9999)}-${randomNumber(0,9999)}-${randomNumber(0, 9999)}-${randomNumber(0, 9999)}`;
  const expirationDate = `${randomNumber(01, 12)}/${randomNumber(21, 35)}`;
  const securityCode = `${randomNumber(000, 999)}`;
  const type = getAgeClient(schema.birth) > 45 ? 'Gold' : 'Classic';

  //armo parametros para actualizar la tabla de usuarios con el nuevo campo creditCard
  const dbParams = {
    ExpressionAttributeNames: {
      '#C': 'creditCard',
    },
    ExpressionAttributeValues: {
      ':c': {
          number:  creditCardNumber,
          expiration: expirationDate,
          ccv:  securityCode,
          type: type,
      },
    },
    Key: {
      dni: schema.dni,
    },
    ReturnValues: 'ALL_NEW',
    UpdateExpression: 'SET #C = :c',
  };

  //llamo al servicio que actualiza el cliente
  await updateClientService(dbParams);


};

module.exports = { createCardDomain };
