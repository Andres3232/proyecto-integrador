
const getAgeClient = require('../helper/getAgeClient');
const { ClientCreatedEvent } = require('../schema/event/clientCreated.event');
const { CreateClientValidation } = require('../schema/input/createClient.input');
const { createClientService } = require('../service/createClient.service');
const { publishClientCreated } = require('../service/publishClientCreated.service');

const createClientDomain = async (commandPayload, commandMeta) => {

  const validation = new CreateClientValidation(commandPayload, commandMeta);
  
  const schema = validation.get();

  if (getAgeClient(schema.birth) < 18 || getAgeClient(schema.birth) > 65) {
    return {
      statusCode: 400,
      body: 'Client must be between 18 and 65 years old',
    };
  }

  params = {...schema, enable: true}
  await createClientService(params);

  await publishClientCreated(new ClientCreatedEvent(schema, commandMeta));

  return {
    statusCode: 201,
    body: 'Client created!',
  };
};

module.exports = { createClientDomain };
