
const {CreateClientEventValidation} = require('../schema/input/createClientEvent.input');
const { updateClientService } = require('../service/updateClient.service');
const { giftChoser } = require('../helper/giftChoser');

const createGiftDomain = async (payload, meta) => {
  // valido el schema

  const validations = new CreateClientEventValidation(JSON.parse(payload.Message),meta);
  const schema = validations.get();  
  const gift = giftChoser(schema.birth)

  const dbParams = {
    ExpressionAttributeNames: {
      "#G": "gift",
    },
    ExpressionAttributeValues: {
      ":g":  gift,
    },
    Key: {
      dni:  schema.dni,
    },
    ReturnValues: "ALL_NEW",
    UpdateExpression: "SET #G = :g",
  };

  //llamo al servicio que actualiza el cliente
  await updateClientService(dbParams);


};

module.exports = { createGiftDomain };
