
const { GetByIdClientValidation } = require('../schema/input/getByIdClient.input');
const { getByIdClientService } = require('../service/getByIdClient.service');
const { updateClientService } = require('../service/updateClient.service');

const deleteClient = async (payload, meta) => {

  const validation = new GetByIdClientValidation(payload, meta);

  const schema = validation.get();
  
  const { Item } = await getByIdClientService({Key: schema});
  
  if(!Item || Item.enable === false){
    return { 
      status: 404, 
      body: { mesasge: 'Client not found'}}
  }

  const dbParams = {
    ExpressionAttributeNames: {
      '#E': 'enable',
    },
    ExpressionAttributeValues: {
      ':e': false
    },
    Key: {
      dni: schema.dni,
    },
    ReturnValues: 'ALL_NEW',
    UpdateExpression: 'SET #E = :e',
  };

  await updateClientService(dbParams);

  return {
    statusCode: 200,
    body: 'Client Deleted',
  };
};

module.exports = { deleteClient };
