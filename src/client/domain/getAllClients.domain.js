
const {  getAllClientsService } = require('../service/getAllClients.service');


const getAllClientsDomain = async () => {

  const clients = await getAllClientsService();

  const clientsActives = clients.filter( client => client.enable === true)

  return {
    statusCode: 200,
    body: {clientsActives},
  };
};

module.exports = { getAllClientsDomain };
