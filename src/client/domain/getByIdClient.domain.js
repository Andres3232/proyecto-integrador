
const { GetByIdClientValidation } = require('../schema/input/getByIdClient.input');
const { getByIdClientService } = require('../service/getByIdClient.service');


const getByIdClient = async (payload, meta) => {


  const validation = new GetByIdClientValidation(payload, meta);
  
  const schema = validation.get();

  const { Item } = await getByIdClientService({Key: schema});

    if(!Item || Item.enable === false){
    return { 
      status: 404, 
      body: { mesasge: 'Client not found'}}
  }

  return {
    statusCode: 201,
    body: {client: Item},
  };
};

module.exports = { getByIdClient };
