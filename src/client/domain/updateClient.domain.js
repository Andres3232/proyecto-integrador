
const { updateExpression } = require('../helper/updateExpression');
const { ClientCreatedEvent } = require('../schema/event/clientCreated.event');
const { GetByIdClientValidation } = require('../schema/input/getByIdClient.input');
const { UpdateClientValidation } = require('../schema/input/updateClient.validation');
const { getByIdClientService } = require('../service/getByIdClient.service');
const { publishClientCreated } = require('../service/publishClientCreated.service');
const { updateClientService } = require('../service/updateClient.service');

const updateClientDomain = async (commandPayload, meta,rawCommand) => {

  const { dni: payloadDni, ...payload } = commandPayload
  const dni = rawCommand?.pathParameters?.dni ?? payloadDni

  const validation = new UpdateClientValidation(payload,meta)
 
  const { Item } = await getByIdClientService({Key: {dni} });

  if(!Item){
    return { 
      status: 404, 
      body: { mesasge: 'Client not found'}}
  }

  const schema = validation.get();

  const params = { ...updateExpression(dni,schema), Key: { dni }, ReturnValues: 'ALL_NEW' };

  if (schema?.birth && schema?.birth !== Item?.birth) {
    const paramsEmit = { ...Item, ...schema }
    delete paramsEmit.gift;
    delete paramsEmit.creditCard;
    await publishClientCreated(new ClientCreatedEvent(paramsEmit, meta));

}

  const result = await updateClientService(params)
  return {
    statusCode: 200,
    body: result.Attributes,
  };
};

module.exports = { updateClientDomain };
