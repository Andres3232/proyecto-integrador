

const updateExpression = (clientId, updateParams)=> {
    
    const Key = { dni: clientId };
    const updateExpressions = [];
    const ExpressionAttributeNames = {};
    const ExpressionAttributeValues = {};

    Object.keys(updateParams).forEach((property) => {
        if (updateParams[property] === undefined) return;
        updateExpressions.push(`#${property}=:${property}`);
        ExpressionAttributeNames[`#${property}`] = property;
        ExpressionAttributeValues[`:${property}`] = updateParams[property];
    });
    if (!updateExpressions.length) {
        throw new Error('Nothing to update');
    }
    const UpdateExpression = `SET ${updateExpressions.join(', ')}`;

    const params = {
        Key,
        UpdateExpression,
        ExpressionAttributeNames,
        ExpressionAttributeValues,
        ReturnValues: 'ALL_NEW',
    };

    return params;
  }

  module.exports = { updateExpression };