
const { InputValidation } = require('nbased/schema/inputValidation');

const schema = {
    dni: { type: String, required: true },
}
class GetByIdClientValidation extends InputValidation {
    constructor(payload, meta) {
        super({
            type: 'CLIENT.CREATE_CLIENT',
            specversion: 'v1.0.0',
            payload,
            source: meta?.source,
            inputSchema: { schema }
        })
    }
}

module.exports = { GetByIdClientValidation }
