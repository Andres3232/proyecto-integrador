
const dynamo = require('nbased/service/storage/dynamo');
const config = require('nbased/util/config');

const TABLE_NAME = config.get('CLIENTS_TABLE');

const createClientService = async (commandPayload) => {
  await dynamo.putItem({
    TableName: TABLE_NAME,
    Item: commandPayload,
  });
};

module.exports = { createClientService };
