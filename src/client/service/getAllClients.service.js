
const dynamo = require('nbased/service/storage/dynamo');

const config = require('nbased/util/config');

const TABLE_NAME = config.get('CLIENTS_TABLE');

const getAllClientsService = async () => {
  const clients = await dynamo.scanTable({ TableName: TABLE_NAME })
  return clients.Items;
};

module.exports = { getAllClientsService };
