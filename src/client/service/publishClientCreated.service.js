
const sns = require('nbased/service/downstream/sns');

const config = require('nbased/util/config');

const CLIENTS_CREATED_TOPIC = config.get('CLIENTS_CREATED_TOPIC');

const publishClientCreated = async (clientCreatedEvent) => {
  const { eventPayload, eventMeta } = clientCreatedEvent.get();

  const snsPublishParams = {
    TopicArn: CLIENTS_CREATED_TOPIC,
    Message: eventPayload,
  };

  await sns.publish(snsPublishParams, eventMeta);
};

module.exports = { publishClientCreated };
