
const dynamo = require('nbased/service/storage/dynamo');
const config = require('nbased/util/config');

const TABLE_NAME = config.get('CLIENTS_TABLE');

module.exports.updateClientService = (params) => dynamo.updateItem({ TableName: TABLE_NAME, ...params })