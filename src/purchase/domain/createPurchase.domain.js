const { CreatePurchaseValidation } = require("../schema/input/createPurchase.input");
const { createPurchaseService } = require("../service/createPurchase.service");
const { v4 } = require("uuid");
const { getByIdClientService } = require("../../client/service/getByIdClient.service");
const { ErrorHandled } = require("nbased/util/error");
const { descByClient } = require("../helper/calculate-discount.helper");
const { updateClientService } = require("../../client/service/updateClient.service");
const { updateExpression } = require("../../client/helper/updateExpression");

const createPurchaseDomain = async (commandPayload, commandMeta) => {

  const validation = new CreatePurchaseValidation(commandPayload, commandMeta);

  const schema = validation.get()

  const {dni} = schema

  const client = await getByIdClientService({Key: {dni}});

  if(!client.Item || client?.Item.enable === false) {
    throw new ErrorHandled('Client Disabled', { status: 401, code: 'UNAUTHORIZED', layer: 'CLIENT'});
  }

  const { purchaseData, clientData } = descByClient(schema, client.Item);

  const purchaseToCreate = { id: v4(), purchaseData }

  await createPurchaseService(purchaseToCreate);

  delete clientData.dni
  const params = { ...updateExpression(dni,clientData), Key: {dni}, ReturnValues: 'ALL_NEW' };

  await updateClientService(params)

  return {
    statusCode: 201,
    body: { message: 'Purchase Created!', purchaseData, clientData },
  };
};

module.exports = { createPurchaseDomain };
