
const { InputValidation } = require('nbased/schema/inputValidation');

const products = {
    name: { type: String, required: true },
    price: { type: Number, required: true }
}

const schema = {
    dni: { type: String, required: true },
    products: { type: [products], required: true, min: 1 },
}
class CreatePurchaseValidation extends InputValidation {
    constructor(payload, meta) {
        super({
            type: 'PURCHASE.CREATE_PURCHASE',
            specversion: 'v1.0.0',
            payload,
            source: meta?.source,
            inputSchema: { schema }
        })
    }
}

module.exports = { CreatePurchaseValidation }