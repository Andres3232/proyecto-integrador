
const dynamo = require('nbased/service/storage/dynamo');
const config = require('nbased/util/config');

const TABLE_NAME = config.get('PURCHASES_TABLE');

const createPurchaseService = async (Item) => {
  await dynamo.putItem({
    TableName: TABLE_NAME,
    Item,
  });
};

module.exports = { createPurchaseService };
